require 'minitest/autorun'
require 'active_record'

I18n.enforce_available_locales = false
puts "Test with ActiveRecord #{ActiveRecord::VERSION::STRING}"

if ActiveRecord::VERSION::MAJOR < 5
  ActiveRecord::Base.raise_in_transactional_callbacks = true
end
ActiveRecord::Base.establish_connection(:adapter => "sqlite3", :database => ":memory:")
ActiveRecord::Migration.verbose = false

require 'acts_as_customizable'

def setup_db
  ActiveRecord::Schema.define(:version => 1) do
    create_table :divisions do |t|
      t.string :name
    end

    create_table :products do |t|
      t.string :name
      t.integer :division_id
    end

    create_table :orders do |t|
      t.string :date
      t.references :product
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
    end

    create_table :custom_categories do |t|
      t.string  :model, :limit => 20, :null => false
      t.integer :position
      t.string  :title,  :limit => 30, :null => false
      t.string :scopeable_id, :limit => 20
      t.string :scopeable_type, :limit => 20
    end

    create_table :custom_fields do |t|
      t.string  :model, :limit => 20, :null => false
      t.integer :custom_category_id
      t.string  :name,  :limit => 30, :null => false
      t.string  :title, :string, :limit => 50
      t.integer :position
      t.string  :condition
      t.string  :field_format, :limit => 30, :null => false
      t.text    :possible_values
      t.string  :regexp
      t.integer :min_length, :default => 0
      t.integer :max_length, :default => 0
      t.boolean :is_required, :default => false, :null => false
      t.datetime :updated_at
      t.string :scopeable_id, :limit => 20
      t.string :scopeable_type, :limit => 20
    end

    create_table :custom_values do |t|
      t.integer :custom_field_id, :null => false
      t.string  :customized_type, :limit => 20, :null => false
      t.integer :customized_id, :null => false

      t.integer :value_integer
      t.string  :value_string
      t.date    :value_date
      t.text    :value_text
      t.decimal :value_decimal, :precision => 8, :scale => 2
      t.float   :value_float
      t.boolean :value_boolean

      t.datetime :updated_at
    end
  end
end

def clear_db
  ActiveRecord::Base.connection.data_sources.each do |table|
    ActiveRecord::Base.connection.execute("DELETE FROM #{table}")
    ActiveRecord::Base.connection.execute("DELETE FROM sqlite_sequence where name='#{table}'")
  end
  ActiveRecord::Base.connection.execute("VACUUM")
end

class Division < ActiveRecord::Base
  has_many :products
end

class Product < ActiveRecord::Base
  belongs_to :division
  has_many :orders
end

class Order < ActiveRecord::Base
  belongs_to :product
  acts_as_customizable :scope => [ :division_id, :product_id ]

  def division_id
    product.division_id if product
  end
end

unless defined? Authorization
  # mockup declarative_authorization
  class CustomField < ActiveRecord::Base
    scope :with_permissions_to, lambda { |args| nil }
  end
end

setup_db
