require 'test_helper'

class CustomFieldTest < MiniTest::Test
  def setup
    @sweets = Division.create! :name => 'Sweets'
    @chocolate = @sweets.products.create! :name => 'Chocolate'
    @jellybaby = @sweets.products.create! :name => 'Jelly Baby'

    @kakao = CustomField.create! :name => 'kakao', :model => 'Order', :field_format => 'int', :scopeable => @chocolate
    @color = CustomField.create! :name => 'color', :model => 'Order', :field_format => 'int', :scopeable => @jellybaby
    @calories = CustomField.create! :name => 'calories', :model => 'Order', :field_format => 'int', :scopeable => @sweets
    @priority = CustomField.create! :name => 'priority', :model => 'Order', :field_format => 'int'
  end

  def teardown
    clear_db
  end

  def test_scope
    # both division and product
    assert_equal [@kakao, @calories, @priority], CustomField.for_scope(Division => @sweets.id, Product => @chocolate.id).to_a
    assert_equal [@color, @calories, @priority], CustomField.for_scope(Division => @sweets.id, Product => @jellybaby.id).to_a

    # only division
    assert_equal [@calories, @priority], CustomField.for_scope(Division => @sweets.id).to_a

    # blank product
    assert_equal [@calories, @priority], CustomField.for_scope(Division => @sweets.id, Product => '').to_a

    # only product
    assert_equal [@kakao, @priority], CustomField.for_scope(Product => @chocolate.id).to_a
    assert_equal [@color, @priority], CustomField.for_scope(Product => @jellybaby.id).to_a

    # nothing
    assert_equal [@priority], CustomField.for_scope({}).to_a

    # unknown values
    assert_equal [@priority], CustomField.for_scope({Division => 123, Product => 456}).to_a
  end

  def test_exclusive_scope
    # product
    assert_equal [@kakao], CustomField.for_exclusive_scope(Product => @chocolate.id).to_a
    assert_equal [@color], CustomField.for_exclusive_scope(Product => @jellybaby.id).to_a

    # division
    assert_equal [@calories], CustomField.for_exclusive_scope(Division => @sweets.id).to_a

    # nothing
    assert_equal [@priority], CustomField.for_exclusive_scope({}).to_a

    # blank division
    assert_equal [@priority], CustomField.for_exclusive_scope({ Division => '' }).to_a
  end
end
