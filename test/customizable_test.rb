require 'test_helper'

class CustomizableTest < MiniTest::Test
  def setup
    @field_number = CustomField.create! :name => 'number', :model => 'Order', :field_format => 'string'
    @field_min_5  = CustomField.create! :name => 'min_5', :model => 'Order', :field_format => 'string', :min_length => 5
    @field_max_10 = CustomField.create! :name => 'max_10', :model => 'Order', :field_format => 'string', :max_length => 5
    @field_bool   = CustomField.create! :name => 'bool', :model => 'Order', :field_format => 'bool'
    @field_date   = CustomField.create! :name => 'date', :model => 'Order', :field_format => 'date'
  end

  def teardown
    clear_db
  end

  def test_customizable
    assert_equal [Order], CustomField.customizable_classes
  end

  def test_empty
    order = Order.create!
    assert_equal [], order.custom_values
    assert_equal 0, order.custom_values.length
  end

  def test_create
    order = Order.create! :custom_number => 'ABC'
    assert_equal "ABC", order.custom_value_for(1).value
    assert_equal 1, order.custom_values.length
  end

  def test_date
    order = Order.create! :custom_date => Date.new(2009,3,1)
    order.reload
    assert_equal true, order.custom_date.is_a?(Date)
    assert_equal '01.03.2009'.to_date, order.custom_date
  end

  def test_scope
    product = Product.create! :name => 'Chocolate'
    field = CustomField.create! :name => 'scoped_field', :model => 'Order', :field_format => 'string', :scopeable => product
    assert_equal 1, field.position
    assert_equal product, field.scopeable

    order = product.orders.create!
    order.update! :custom_scoped_field => 'bar'

    assert_equal 6, order.available_custom_fields.size
    assert_equal true, order.available_custom_fields.include?(field)
    assert_equal 'bar', order.custom_scoped_field
  end

  def test_add
    order = Order.create!
    order.custom_number = "ABC-1234"
    order.save!
    assert_equal "ABC-1234", order.custom_values.first.value

    order.update! :custom_min_5 => "12345"
    assert_equal "ABC-1234", order.custom_value_for(@field_number).value
    assert_equal "12345", order.custom_value_for(@field_min_5).value
  end

  def test_saving_not_null_values
    order = Order.create! :custom_number => 'ABC-1234', :custom_min_5 => nil
    order.reload
    assert_equal 1, order.custom_values.size
  end

  def test_update
    order = Order.create! :custom_number => 'ABC'
    order.custom_number = 'XYZ'
    order.save!
    order = Order.find(order.id)
    assert_equal "XYZ", order.custom_value_for(@field_number).value
  end

  def test_delete
    order = Order.create! :custom_number => 'ABC'
    assert_equal 'ABC', order.custom_values.first.value
    order.custom_number = nil
    order.save!
    assert_nil order.custom_value_for(@field_number)
  end

  def test_validation_min_length_on_new_record
    order = Order.new :custom_min_5 => 'ABC'
    assert_equal false, order.save
    #assert_equal "is invalid", order.errors[:custom_values]
  end

  def test_validation_min_length_on_existing_record
    order = Order.create!
    assert_equal false, order.update(:custom_min_5 => 'ABC')
    #assert_equal "is invalid", order.errors[:custom_values]
  end

  def test_validation_max_length
    order = Order.new :custom_max_10 => '123456'
    assert_equal false, order.valid?
    assert_equal false, order.save
  end

  def test_true
    order = Order.new :custom_bool => true
    assert_equal true, order.custom_value_for(@field_bool).value
  end

  def test_method_missing_getter_with_value
    order = Order.new :custom_number => 'ABC'
    assert_equal 'ABC', order.custom_number
  end

  def test_method_missing_getter_without_value
    order = Order.new
    assert_nil order.custom_number
  end

  def test_method_missing_setter
    order = Order.new
    order.custom_number = 'DEF'
    order.save!
    assert_equal "DEF", order.custom_number
  end

  def test_update!
    order = Order.create!
    order.update! :custom_number => '1234', :custom_min_5 => '45678'
    order = Order.find(order.id)
    assert_equal "1234", order.custom_number
    assert_equal "45678", order.custom_min_5
  end

  def test_finder_and_scope
    order = Order.create! :custom_number => 'abc', :custom_min_5 => '12345'
    Order.create! :custom_number => 'def', :custom_min_5 => '12345'
    Order.create! :custom_number => 'abc', :custom_min_5 => '67890'

    assert_equal [order], Order.find_by_custom_values(@field_number.id => 'abc', @field_min_5.id => '12345')
    assert_equal [order], Order.with_custom_value(:field_id => @field_number.id, :value => 'abc').with_custom_value(:field_id => @field_min_5.id, :value => '12345')
  end

  def test_condition
    field1 = CustomField.create! :name => 'condition_test1', :model => 'Order', :field_format => 'bool', :condition => 'self.is_a?(Order)'
    field2 = CustomField.create! :name => 'condition_test2', :model => 'Order', :field_format => 'bool', :condition => 'false'
    order = Order.new

    assert order.available_custom_fields.include?(field1)
    assert !order.available_custom_fields.include?(field2)
  end

  def test_name_conflict_with_other_custom_fields
    c = CustomField.new :name => 'number', :model => 'Order', :field_format => 'string'
    assert_equal false, c.valid?
    assert_equal true, c.errors[:name].present?
  end

  def test_name_validation
    c = CustomField.new :name => 'Totally wrong name'
    assert_equal false, c.valid?
    assert_equal true, c.errors[:name].present?
  end
end
