# encoding: utf-8
class CustomValue < ActiveRecord::Base
  belongs_to :custom_field
  belongs_to :customized, :polymorphic => true

  validates_presence_of :value
  validate :check_for_errors

  def self.value_field_name(format)
    { 'int'    => 'value_integer',
      'float'  => 'value_float',
      'decimal'=> 'value_decimal',
      'date'   => 'value_date',
      'list'   => 'value_string',
      'string' => 'value_string',
      'text'   => 'value_text',
      'bool'   => 'value_boolean'
    }[format]
  end

  def value=(val)
    send "#{CustomValue.value_field_name(custom_field.field_format)}=", val
  end

  def value
    # Don't lookup custom_field (because of additional query),
    # instead look for only one field which is not null
    self.value_integer ||
    self.value_float ||
    self.value_decimal ||
    self.value_date ||
    self.value_string ||
    self.value_text ||
    self.value_boolean
  end

private

  def check_for_errors
    if value.blank?
      errors.add(:value, 'darf nicht leer sein') if custom_field.is_required?
    else
      errors.add(:value, 'ist ungültig') unless custom_field.regexp.blank? or value =~ Regexp.new(custom_field.regexp)
      errors.add(:value, 'ist zu kurz') if custom_field.min_length > 0 and value.length < custom_field.min_length
      errors.add(:value, 'ist zu lang') if custom_field.max_length > 0 and value.length > custom_field.max_length

      # Format specific validations
      case custom_field.field_format
        when 'list'
          errors.add(:value, 'ist ungültig') unless custom_field.possible_values.include?(value)
      end
    end
  end
end
