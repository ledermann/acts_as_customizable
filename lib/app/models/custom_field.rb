class CustomField < ActiveRecord::Base
  has_many :custom_values, :dependent => :delete_all
  belongs_to :custom_category

  acts_as_list :scope => 'model = \'#{self.model}\' AND ' +
                         'scopeable_id #{self.scopeable_id ? (\'= \' + %Q{"} + self.scopeable_id.to_s + %Q{"}) : \'IS NULL\'} AND ' +
                         'scopeable_type #{self.scopeable_type ? (\'= \' + %Q{"} + self.scopeable_type.to_s + %Q{"}) : \'IS NULL\'} AND ' +
                         'custom_category_id #{self.custom_category_id ? (\'= \' + self.custom_category_id.to_s) : \'IS NULL\'}'

  FIELD_FORMATS = { 'string' => 'Alphanumerisch',
                    'text'   => 'Memo',
                    'int'    => 'Numerisch (Ganzzahl)',
                    'float'  => 'Numerisch (Gleitkomma)',
                    'decimal'=> 'Numerisch (Dezimal)',
                    'list'   => 'Liste',
                    'date'   => 'Datum',
                    'bool'   => 'Logisch' }.freeze

  validates_presence_of :name, :field_format, :model
  validates_uniqueness_of :name, :scope => :model, :case_sensitive => true
  validates_length_of :name, :maximum => 50
  validates_format_of :name, :with => /\A[a-z]+[a-z0-9_]*\z/
  validates_inclusion_of :field_format, :in => FIELD_FORMATS.keys

  scope :for_model, lambda { |model| where(:model => model) }

  scope :for_scope, lambda { |scope_hash|
    # param is a hash like { Division => 42, Product => 4711 }
    raise ArgumentError unless scope_hash.is_a?(Hash)

    conditions = ['(scopeable_id IS NULL)']
    scope_hash.each_pair do |klass, value|
      conditions << "(scopeable_id = '#{value}' AND scopeable_type = '#{klass.base_class}')" if value.present?
    end

    where(conditions.join(' OR '))
  }

  scope :for_exclusive_scope, lambda { |scope_hash|
    # param is a hash like { Product_id => 4711 }, max. one key allowed!
    raise ArgumentError unless scope_hash.is_a?(Hash)
    raise ArgumentError if scope_hash.keys.size > 1

    conditions = 'scopeable_id IS NULL'
    if klass = scope_hash.keys.first
      value = scope_hash.values.first
      conditions = "(scopeable_id = '#{value}' AND scopeable_type = '#{klass.base_class}')" if value.present?
    end

    where(conditions)
  }

  class_attribute :customizable_classes
  self.customizable_classes = []

  validate :check_for_errors
  def check_for_errors
    if self.field_format == 'list'
      errors.add(:possible_values, 'darf nicht leer sein') if self.possible_values.blank?
    end
  end

  def <=>(field)
    position <=> field.position
  end

  def to_s
    return 'Neues Zusatzfeld' if new_record?
    title.to_s
  end

  def model_klass
    self.model.constantize
  end

  def validation_rules
    [ ("min. #{min_length}" if min_length > 0),
      ("max. #{max_length}" if max_length > 0),
      ("RegExp: #{regexp}" unless regexp.blank?),
      (possible_values unless possible_values.blank?)
      ].compact.join(', ')
  end

  def list_options
    self.possible_values.split(',')
  end

  def scopeable
    scopeable_type.constantize.find(scopeable_id) if scopeable_id && scopeable_type
  end

  def scopeable=(value)
    if value.nil?
      self.scopeable_id = nil
      self.scopeable_type = nil
    elsif value.respond_to?(:id) && value.class.respond_to?(:base_class)
      self.scopeable_id = value.id.to_s
      self.scopeable_type = value.class.base_class.to_s
    else
      raise ArgumentError
    end
  end
end
