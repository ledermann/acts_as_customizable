class CustomCategory < ActiveRecord::Base
  has_many :custom_fields, :dependent => :nullify

  acts_as_list :scope => 'model = \'#{self.model}\' AND ' +
                         'scopeable_id #{self.scopeable_id ? (\'= \' + %Q{"} +self.scopeable_id.to_s + %Q{"}) : \'IS NULL\'} AND '+
                         'scopeable_type #{self.scopeable_type ? (\'= \' + %Q{"} + self.scopeable_type.to_s + %Q{"}) : \'IS NULL\'}'

  validates_presence_of :title

  scope :for_model, lambda { |model| where(:model => model) }
  scope :for_scope, lambda { |scope_hash|
    # param is a hash like { Division => 42, Product => 4711 }
    raise ArgumentError unless scope_hash.is_a?(Hash)

    conditions = ['(scopeable_id IS NULL)']
    scope_hash.each_pair do |klass, value|
      conditions << "(scopeable_id = '#{value}' AND scopeable_type = '#{klass.base_class}')" if value.present?
    end

    where(conditions.join(' OR '))
  }

  scope :for_exclusive_scope, lambda { |scope_hash|
    # param is a hash like { Product => 4711 }, max. one key allowed!
    raise ArgumentError unless scope_hash.is_a?(Hash)
    raise ArgumentError if scope_hash.keys.size > 1

    if klass = scope_hash.keys.first
      value = scope_hash.values.first
      conditions = "(scopeable_id = '#{value}' AND scopeable_type = '#{klass.base_class}')"
    else
      conditions = 'scopeable_id IS NULL'
    end

    where(conditions)
  }

  def to_s
    self.title.to_s
  end

  def scopeable
    scopeable_type.constantize.find(scopeable_id) if scopeable_id && scopeable_type
  end

  def scopeable=(value)
    if value.nil?
      self.scopeable_id = nil
      self.scopeable_type = nil
    elsif value.respond_to?(:id) && value.class.respond_to?(:base_class)
      self.scopeable_id = value.id.to_s
      self.scopeable_type = value.class.base_class.to_s
    else
      raise ArgumentError
    end
  end
end
