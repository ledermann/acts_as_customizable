require 'acts_as_customizable/version'
require 'acts_as_list'

module ActsAsCustomizable
  module ClassMethods
    def acts_as_customizable(options={})
      return if self.included_modules.include?(InstanceMethods)

      class_attribute :customizable_options
      self.customizable_options = options

      # Remember classes which are customizable
      unless CustomField.respond_to?(:customizable_classes)
        CustomField.class_attribute :customizable_classes
        CustomField.customizable_classes = []
      end
      unless CustomField.customizable_classes.include?(self)
        CustomField.customizable_classes << self
      end

      has_many :custom_values, :as => :customized, :dependent => :delete_all, :autosave => true

      # Save custom values when saving the customized object
      before_validation :remove_empty_values

      # Scope to filter for a given custom_field/value combination
      # Example: Contact.with_custom_value(:field_id => 10, :value => 'foo')
      scope :with_custom_value, lambda { |opts|
        field = CustomField.with_permissions_to(:read).find(opts[:field_id])
        raise "Field #{opts[:field_id]} not found!" unless field
        value_field_name = CustomValue.value_field_name(field.field_format)

        joins("INNER JOIN custom_values cv_#{field.id} ON
                                       (cv_#{field.id}.customized_id = #{self.table_name}.id AND
                                        cv_#{field.id}.customized_type = '#{self.base_class.name}' AND
                                        cv_#{field.id}.custom_field_id = #{field.id} AND
                                        cv_#{field.id}.#{value_field_name} = '#{opts[:value]}')")
      }

      send :include, InstanceMethods
    end

    # Searching for multiple custom values (using to the named_scope above)
    def find_by_custom_values(value_hash)
      scop = self
      value_hash.each_pair do |k,v|
        scop = scop.with_custom_value(:field_id => k, :value => v)
      end
      scop.to_a
    end

    def customizable_scope
      customizable_options[:scope]
    end
  end

  module InstanceMethods
    def self.included(base)
      base.extend ClassMethods
    end

    def respond_to?(method_name, include_private = false)
      if m = method_name.to_s.match(/\Acustom\_([^=]*)(=?)/)
        return true if available_custom_fields.detect { |c| c.name.underscore == m[1] }
      end

      super
    end

    # Getter and Setter for use
    # custom_{name} = 'ABC'
    # custom_{name} # => 'ABC'
    def method_missing(method_name, *args)
      if m = method_name.to_s.match(/\Acustom\_(.*)=/)
        if custom_field = available_custom_fields.detect { |c| c.name == m[1] }
          return send(:custom_field_values=, { custom_field.id => args.first })
        end
      elsif m = method_name.to_s.match(/\Acustom\_(.*)/)
        if custom_field = available_custom_fields.detect { |c| c.name == m[1] }
          return custom_value_for(custom_field).try(:value)
        end
      end
      super
    end

    def available_custom_fields
      @available_custom_fields ||= begin
        scope_hash = {}
        if scope_fields = Array(customizable_options[:scope])
          scope_fields.each do |field|
            if assoc = self.class.reflect_on_all_associations(:belongs_to).find { |a| a.foreign_key == field.to_s }
              # field name is part of a :belongs_to association => Use klass from association
              key = assoc.klass
            else
              # Try to build klass from field name (e.g. :product_id -> "Product")
              key = field.to_s.sub(/_id$/,'').classify.constantize
            end

            scope_hash[key] = self.send(field)
          end
        end

        all_fields = CustomField.
                       for_model(self.class.name).
                       for_scope(scope_hash).
                       order(:position).
                       with_permissions_to(:read)

        # Result only fields matching the condition (which is defined as Ruby code)
        result = []
        all_fields.each do |f|
          result << f if f.condition.blank? || self.instance_eval(f.condition)
        end
        result
      end
    end

    def custom_field_values
      @custom_field_values ||= available_custom_fields.collect { |f|
                                 custom_values.detect { |v|
                                   v.custom_field_id == f.id && (v.custom_field = f) # Hack to prevent extra loading
                                 } ||
                                 custom_values.build(:custom_field => f)
                               }
    end

    def custom_value_for(c)
      field_id = (c.is_a?(CustomField) ? c.id : c.to_i)
      custom_values.detect {|v| v.custom_field_id == field_id }
    end

    def custom_field_values=(values)
      @custom_field_values_changed = true
      values = values.stringify_keys
      custom_field_values.each do |custom_value|
        custom_value.value = values[custom_value.custom_field_id.to_s] if values.has_key?(custom_value.custom_field_id.to_s)
      end if values.is_a?(Hash)
    end

    def custom_field_values_changed?
      @custom_field_values_changed ||= false
      @custom_field_values_changed == true
    end

    def remove_empty_values
      if custom_field_values_changed?
        custom_field_values.each do |cv|
          if cv.value.blank?
            cv.mark_for_destruction
          end
        end
        @custom_field_values_changed = false
        @custom_field_values = nil
      end
      true
    end
  end
end

ActiveRecord::Base.extend(ActsAsCustomizable::ClassMethods)

if defined?(Rails::Railtie)
  class Railtie < Rails::Railtie
    initializer 'acts_as_customizable.init', :after => 'acts_as_list.insert_into_active_record' do
      require 'app/models/custom_field'
      require 'app/models/custom_value'
      require 'app/models/custom_category'
    end
  end
else
  require 'app/models/custom_field'
  require 'app/models/custom_value'
  require 'app/models/custom_category'
end
