# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require 'acts_as_customizable/version'

Gem::Specification.new do |s|
  s.name        = "acts_as_customizable"
  s.version     = ActsAsCustomizable::VERSION
  s.authors     = ["Georg Ledermann"]
  s.email       = ["mail@georg-ledermann.de"]
  s.homepage    = ""
  s.summary     = %q{}
  s.description = %q{}

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency 'activerecord', '>= 5'
  s.add_dependency 'acts_as_list'

  s.add_development_dependency 'rake'
  s.add_development_dependency 'sqlite3'
end
